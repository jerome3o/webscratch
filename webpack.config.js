module.exports = {
    entry: './src/index.js',
    mode: 'development',
    output: {
        filename: 'main.js'
    },
    devServer: {
        contentBase: './public',
        port: 9000,
        watchContentBase: true,
    }
}